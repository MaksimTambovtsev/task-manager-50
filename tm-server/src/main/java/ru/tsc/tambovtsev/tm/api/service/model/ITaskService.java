package ru.tsc.tambovtsev.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @Nullable
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @Nullable
    List<Task> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    List<Task> findAll();

}
