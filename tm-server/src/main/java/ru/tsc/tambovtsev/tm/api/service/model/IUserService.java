package ru.tsc.tambovtsev.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.User;

import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User removeByLogin(@Nullable String login);

    boolean isLoginExists(@Nullable String login);

    boolean isEmailExists(@Nullable String email);

    @Nullable
    User setPassword(@Nullable String userId, @Nullable String password);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

    @Nullable
    List<User> findAll();

}
